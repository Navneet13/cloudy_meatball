//
//  ViewController.swift
//  Cloudy_MeatBall
//
//  Created by navpreet on 2019-11-03.
//  Copyright © 2019 navneet. All rights reserved.
//

import UIKit
import WatchConnectivity

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if WCSession.isSupported() {
            WCSession.default.delegate = self
            WCSession.default.activate()
        }
        else {
            print("Phone does not support WCSession")
        }
        
        getCurrentCity()
    }
    
    func getCurrentCity() {
        
        let timeZoneIdentifiers = TimeZone.knownTimeZoneIdentifiers
        var allCities: [String] = []
        for identifier in timeZoneIdentifiers {
            if let cityName = identifier.split(separator: "/").last {
                allCities.append("\(cityName)")
            }
        }
        
        print(allCities)
        
        let dictionary = ["cityArray": allCities]
        if WCSession.default.isReachable {
            WCSession.default.sendMessage(dictionary, replyHandler: nil)
        }
        else {
            print("Watch is not reachable")
        }
    }
}

extension ViewController: WCSessionDelegate {
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any], replyHandler: @escaping ([String : Any]) -> Void) {
        
        // 1. When a message is received from the watch, output a message to the UI
        // NOTE: Since session() runs in background, you cannot directly update UI from the background thread.
        // Therefore, you need to wrap any UI updates inside a DispatchQueue for it to work properly.
        DispatchQueue.main.async {
            print("\nMessage Received: \(message)")
        }
        
        // 2. Also, print a debug message to the phone console
        // To make the debug message appear, see Moodle instructions
        print("Received a message from the watch: \(message)")
    }
    
}
