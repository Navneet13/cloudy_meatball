//
//  CityRow.swift
//  Cloudy_MeatBall WatchKit Extension
//
//  Created by navpreet on 2019-11-04.
//  Copyright © 2019 navneet. All rights reserved.
//

import Foundation
import WatchKit

class CityRow: NSObject {
    
    @IBOutlet weak var cityName: WKInterfaceLabel!
    
}
