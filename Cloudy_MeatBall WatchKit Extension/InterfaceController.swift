//
//  InterfaceController.swift
//  Cloudy_MeatBall WatchKit Extension
//
//  Created by navpreet on 2019-11-03.
//  Copyright © 2019 navneet. All rights reserved.
//

import WatchKit
import Foundation
import Alamofire
import SwiftyJSON
import CoreLocation
import WatchConnectivity

class InterfaceController: WKInterfaceController, CLLocationManagerDelegate {
   
    @IBOutlet weak var cityTableView: WKInterfaceTable!
    @IBOutlet weak var timeLabel: WKInterfaceLabel!
    @IBOutlet weak var chooseCityButton: WKInterfaceButton!
    @IBOutlet weak var weather: WKInterfaceLabel!
    
    var locationManager = CLLocationManager()
    
    var cityArray = [String]()
    
    override func awake(withContext context: Any?) {
       
        super.awake(withContext: context)
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        if WCSession.isSupported() {
            let wcsession = WCSession.default
            wcsession.delegate = self
            wcsession.activate()
        }
        
        getCurrentCity()
        // self.fetchWeather(cityName: "Toronto,ca")
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    
    @IBAction func chooseCityButtonPressed() {
        cityTableView.setHidden(false)
    }
    
    // With Alamofire
    func fetchWeather(cityName: String) {
       
        let urlString = "https://api.openweathermap.org/data/2.5/weather?q=\(cityName)&appid=28bc9fe659b94ca83b55b309feed5705"
       
        Alamofire.request("\(urlString)").responseJSON { (responseData) -> Void in
            if((responseData.result.value) != nil) {
                let swiftyJsonVar = JSON(responseData.result.value!)
                let temperature = swiftyJsonVar["main"].dictionary
                self.weather.setText("Weather: \(temperature?["temp"] ?? "No Temperature") F")
            } else {
                print("Error Occurred")
            }
        }
    }
}

extension InterfaceController: WCSessionDelegate {
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {}
}

extension InterfaceController {
    
    func getCurrentCity() {
        
        let timeZoneIdentifiers = TimeZone.knownTimeZoneIdentifiers
        var allCities: [String] = []
        for identifier in timeZoneIdentifiers {
            if let cityName = identifier.split(separator: "/").last {
                allCities.append("\(cityName)")
            }
        }
        
        self.cityArray = allCities
        setupTable()
    }
    
    func setupTable() {
        cityTableView.setNumberOfRows(cityArray.count, withRowType: "CityRow")
        
        var i = 0
        for city in  self.cityArray {
            if let row = cityTableView.rowController(at: i) as? CityRow {
                row.cityName.setText(city)
            }
            i = i + 1
        }
    }
    
    override func table(_ table: WKInterfaceTable, didSelectRowAt rowIndex: Int) {
        
        let selectedCity = cityArray[rowIndex]
        cityTableView.setHidden(true)
        
        self.chooseCityButton.setTitle(selectedCity)
        self.fetchWeather(cityName: selectedCity)
        self.fetchTime(selectedCity: selectedCity)
    }
    
    func fetchTime(selectedCity: String)  {
        var selectedTimeZone = ""
        let timeZoneIdentifiers = TimeZone.knownTimeZoneIdentifiers
        for identifier in timeZoneIdentifiers {
            if let cityName = identifier.split(separator: "/").last, selectedCity == cityName {
                selectedTimeZone = identifier
            }
        }
       
        // Find the correst abbr of tome zone and get time
        
        let timeZoneDict = NSTimeZone.abbreviationDictionary
        let key = (timeZoneDict as NSDictionary).allKeys(for: selectedTimeZone) as? [String]
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: (key?[0])!)
        dateFormatter.dateFormat = "hh:mm"
        let str = dateFormatter.string(from: date)
        
        timeLabel.setText(str)
    }
}
